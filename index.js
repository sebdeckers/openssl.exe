const {join} = require('path')

module.exports.exe = join(__dirname, 'bin/openssl.exe')

module.exports.cnf = join(__dirname, 'openssl.cnf')
