# openssl.exe

Windows binaries of the OpenSSL runtime and its libraries.

The OpenSSL project does not provide official binaries. These are provided without any endorsement, support, or affiliation with the OpenSSL project. Use at your own peril.

## Usage

Since this package will only install on Windows, it is recommended to include it in your project as an optional dependency.

```
npm install --save-optional openssl.exe
```

## Publisher

https://indy.fulgan.com/SSL/

## License Details

See: [bin/OpenSSL License.txt](./bin/OpenSSL%20License.txt)
